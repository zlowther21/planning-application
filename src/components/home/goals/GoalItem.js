import React, { PropTypes } from 'react';

const GoalItem = (props) => {
    return (
        <li className="list-group-item">
            {props.children} 
            <span className="edit-icon glyphicon glyphicon-pencil pull-right" aria-hidden="true" />
        </li>
    );
};

GoalItem.propTypes = {
    children: PropTypes.node,
};

export default GoalItem;
