import ActionTypes from '../actions/actionTypes';

export default function goalReducer(state = [], action) {
    switch (action.type) {
    case ActionTypes.ADD_GOAL:
        return [...state, Object.assign({}, action.goal)];
    default:
        return state;
    }
}