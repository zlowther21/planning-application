import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import Table from '../../helpers/Table';
import Button from '../../helpers/Button';
import getDaysOfTheWeek from '../../../models/Calendar';

const Meetings = (props) => {
    return (
        <div className="inner-container">
            <h1 className="section-title">{props.sectionTitle}</h1>
            <Table tableHeaders={getDaysOfTheWeek()} />
            <Button success>Add Meeting</Button>
        </div>
    );
};

Meetings.propTypes = {
    sectionTitle: PropTypes.string.isRequired,
};

export default Meetings;


