import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import Table from './Table';

describe('<Table tableHeaders={[1, 2, 3]}/>', () => {
    describe('#render', () => {
        it('should render a table with 3 table headers"', () => {
            const wrapper = shallow(<Table tableHeaders={[1, 2, 3]} />);
            expect(wrapper.find('th')).to.have.length(3);
        });
        it('should render a table header values being respectively: 1, 2, 3 "', () => {
            const wrapper = shallow(<Table tableHeaders={[1, 2, 3]} />);
            expect(wrapper.find('th').nodes[0].props.children).to.equal(1);
            expect(wrapper.find('th').nodes[1].props.children).to.equal(2);
            expect(wrapper.find('th').nodes[2].props.children).to.equal(3);
        });
    });
});