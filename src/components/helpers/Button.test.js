import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import Button from './Button';

describe('Button', () => {
    describe('#render', () => {
        it('<Button> should return a button with class btn-primary"', () => {
            const wrapper = shallow(<Button>Children</Button>);
            expect(wrapper.find('button.btn-primary')).to.have.length(1);
        });
        it('<Button primary> should return a button with class btn-primary"', () => {
            const wrapper = shallow(<Button primary>Children</Button>);
            expect(wrapper.find('button.btn-primary')).to.have.length(1);
        });
        it('<Button default> should return a button with class btn-default"', () => {
            const wrapper = shallow(<Button default>Children</Button>);
            expect(wrapper.find('button.btn-default')).to.have.length(1);
        });
        it('<Button success> should return a button with class btn-success"', () => {
            const wrapper = shallow(<Button success>Children</Button>);
            expect(wrapper.find('button.btn-success')).to.have.length(1);
        });
        it('<Button info> should return a button with class btn-info"', () => {
            const wrapper = shallow(<Button info>Children</Button>);
            expect(wrapper.find('button.btn-info')).to.have.length(1);
        });
        it('<Button warning> should return a button with class btn-warning"', () => {
            const wrapper = shallow(<Button warning>Children</Button>);
            expect(wrapper.find('button.btn-warning')).to.have.length(1);
        });
        it('<Button danger> should return a button with class btn-danger"', () => {
            const wrapper = shallow(<Button danger>Children</Button>);
            expect(wrapper.find('button.btn-danger')).to.have.length(1);
        });
        it('should should say that children are undefined if using <Button />', () => {
            const wrapper = shallow(<Button />);
            expect(wrapper.find('button').node.props.children).to.equal(undefined);
        });
        it('should should say that children are "Hello World" if <Button>Hello World</Button>', () => {
            const wrapper = shallow(<Button>Hello World</Button>);
            expect(wrapper.find('button').node.props.children).to.equal('Hello World');
        });
    });
});