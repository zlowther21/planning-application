import chalk from 'chalk';

export const successChalk = chalk.green;
export const warningChalk = chalk.yellow;
export const errorChalk = chalk.red;
export const infoChalk = chalk.blue;

