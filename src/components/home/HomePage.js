import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Meetings from './meetings/Meetings';
import Goals from './goals/Goals';
import Todo from './todo/Todo';
import * as goalActions from '../../actions/goalAction';

/* eslint-disable react/prefer-stateless-function */

class HomePage extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {

        };

        this.onClickAddGoal = this.onClickAddGoal.bind(this);
    }

    onClickAddGoal(goal) {
        this.props.actions.addGoal(goal);
    }
    render() {
        return (
            <div>
                <div className="row">
                    <section className="col-md-10">
                        <Meetings sectionTitle="Meetings" />
                        <Todo sectionTitle="To Do List" />
                    </section>
                    <section className="col-md-6">
                        <Goals sectionTitle="Goals" goals={this.props.goals} addGoal={this.onClickAddGoal} />
                    </section>
                </div>
            </div>
        );
    }
}

HomePage.propTypes = {
    actions: PropTypes.object.isRequired,
    goals: PropTypes.array.isRequired,
};
function mapStateToProps(state, ownProps) {
    return {
        goals: state.goals,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(goalActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
