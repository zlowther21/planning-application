/**
 * Require Browsersync along with webpack and middleware for it
 */
import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../webpack.config.dev';

const bundler = webpack(config);

/**
 * Run Browsersync and use middleware for Hot Module Replacement
 */
browserSync({
    server: {
        baseDir: 'src',

        middleware: [
            historyApiFallback(),
            webpackDevMiddleware(bundler, {
                // IMPORTANT: dev middleware can't access config, so we should
                // provide publicPath by ourselves
                publicPath: config.output.publicPath,

                noInfo: false,
                quiet: false,
                stats: {
                    assets: false,
                    colors: true,
                    version: false,
                    hash: false,
                    timings: false,
                    chunks: false,
                    chunkModules: false,
                },
            }),

            // bundler should be the same as above
            webpackHotMiddleware(bundler),
        ],
    },

    // no need to watch '*.js' here, webpack will take care of it for us,
    // including full page reloads if HMR won't work
    files: [
        'src/*.html',
    ],
});
