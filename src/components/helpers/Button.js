import React, { PropTypes } from 'react';

const ButtonTypes = {
    success: 'btn-success',
    warning: 'btn-warning',
    danger: 'btn-danger',
    info: 'btn-info',
    primary: 'btn-primary',
    default: 'btn-default',
};

// Even if someone sets more than one of these on a button,
// it will always set to the first in this if/else clause below
const getButtonType = (props) => {
    if (props.success) return ButtonTypes.success;
    else if (props.info) return ButtonTypes.info;
    else if (props.danger) return ButtonTypes.danger;
    else if (props.warning) return ButtonTypes.warning;
    else if (props.primary) return ButtonTypes.primary;
    else if (props.default) return ButtonTypes.default;
    
    return ButtonTypes.primary;
};

const Button = (props) => {
    const type = getButtonType(props);
    return (
        <button type="button" className={`btn ${type}`}>{props.children}</button>
    );
};

/* eslint-disable react/no-unused-prop-types */
Button.propTypes = {
    children: PropTypes.node.isRequired,
    success: PropTypes.bool,
    info: PropTypes.bool,
    danger: PropTypes.bool,
    warning: PropTypes.bool,
    primary: PropTypes.bool,
    default: PropTypes.bool,
};
Button.defaultProps = {
    type: ButtonTypes.success,
};

export default Button;
