import React, { PropTypes } from 'react';
import {Link} from 'react-router';

const Table = (props) => {
    const striped = props.striped ? 'table-striped' : '';
    return (
        <table className={`table ${striped} table-hover`}>
            <thead>
                <tr>
                    {props.tableHeaders.map((header) => {
                        return (
                            <th key={header}>{header}</th>
                        );
                    })}
                </tr>
            </thead>
            <tbody>
                <tr>

                </tr>
            </tbody>
        </table>


    );
};

Table.propTypes = {
    striped: PropTypes.bool,
    tableHeaders: PropTypes.array.isRequired,
};

Table.defaultProps = {
    striped: false,
};

export default Table;

