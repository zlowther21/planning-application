import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import Table from '../../helpers/Table';
import Button from '../../helpers/Button';

const Todo = (props) => {
    return (
        <div className="inner-container">
            <h1 className="section-title">{props.sectionTitle}</h1>
            <Table tableHeaders={['Item', 'Category', 'Priority', 'Status']} />
            <Button success>Add Task</Button>
        </div>
    );
};

Todo.propTypes = {
    sectionTitle: PropTypes.string.isRequired,
};

export default Todo;