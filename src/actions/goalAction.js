/* eslint-disable import/prefer-default-export */
import ActionTypes from './actionTypes';

export function addGoal(goal) {
    return { type: ActionTypes.ADD_GOAL, goal};
}