import {combineReducers} from 'redux';
import goals from './goalReducer';

const rootReducer = combineReducers({
    goals,
});

export default rootReducer;
