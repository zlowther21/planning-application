import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import autofill from 'react-autofill';
import Button from '../../helpers/Button';
import GoalItem from './GoalItem';


const newGoalItem = (goal, index) => {
    return <GoalItem key={index}>{goal.title}</GoalItem>;
};

class Goals extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            newGoal: { title: '' },
        };

        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleTextChange(event) {
        const newGoal = this.state.newGoal;
        newGoal.title = event.target.value;
        this.setState({ newGoal });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.newGoal.title.trim() !== '') {
            this.props.addGoal(this.state.newGoal);
            this.setState({ newGoal: { title: '' }});
        }
    }


    render() {
        return (
            <div className="inner-container">
                <h1 className="section-title">{this.props.sectionTitle}</h1>
                <ul className="list-group">
                    { this.props.goals.map(newGoalItem) }
                    <li className="list-group-item">
                        <form onSubmit={this.handleSubmit} role="form">
                            <label className="sr-only" htmlFor="addGoalInput">Add a Goal</label>
                            <div>
                                <div className="input-group">
                                    <input name="addGoalInput" value={this.state.newGoal.title} onChange={this.handleTextChange} type="text" className="form-control" aria-label="Add a Goal" placeholder="Add a Goal..." />
                                    <div className="input-group-btn">
                                        <button type="submit" className="btn btn-success">Add</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        );
    }
}

Goals.propTypes = {
    sectionTitle: PropTypes.string.isRequired,
    goals: PropTypes.array.isRequired,
    addGoal: PropTypes.func.isRequired,
};

export default autofill(Goals);