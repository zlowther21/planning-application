/* 
    Pattern adapted from: https://github.com/react-bootstrap/react-router-bootstrap/blob/master/src/LinkContainer.js

    From react-router docs: 
    The <Link> will be active if the current route is either 
    the linked route or any descendant of the linked route. 
    To have the link be active only on the exact linked route, 
    use <IndexLink> instead or set the onlyActiveOnIndex prop.
*/

import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';

const NavbarLinkContainer = (props, context) => {
    const isActive = context.router.isActive(props.to, props.onlyActiveOnIndex);
    const NavbarLink = props.onlyActiveOnIndex ? IndexLink : Link;
    const className = isActive ? 'active' : '';
    return (
        <li className={className}>
            <NavbarLink to={props.to}>{props.children}</NavbarLink>
        </li>
    );
};

NavbarLinkContainer.propTypes = {
    to: PropTypes.string,
    onlyActiveOnIndex: PropTypes.bool,
    children: PropTypes.node,
};

NavbarLinkContainer.contextTypes = {
    router: PropTypes.object.isRequired,
};

export default NavbarLinkContainer;