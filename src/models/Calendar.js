const getDaysOfTheWeek = (locale) => {
    return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
};

export default getDaysOfTheWeek;
