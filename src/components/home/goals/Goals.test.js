import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import Goals from './Goals';
import GoalItem from './GoalItem';

describe('<Goals />', () => {
    describe('#render', () => {
        it('should return a li with class "list-group-item"', () => {
            const wrapper = shallow(<Goals />);
            expect(wrapper.find('GoalItem')).to.have.length(3);
        });
    });
});