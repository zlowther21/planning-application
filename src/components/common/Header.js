import React from 'react';
import { Link, IndexLink } from 'react-router';
import NavbarLinkContainer from '../helpers/NavLinkContainer';

/* eslint-disable max-len */

const Header = () => {
    return (
        <nav className="navbar navbar-default navbar-static-top">
            <div className="container-fluid">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <IndexLink to="/" className="navbar-brand">Planning Application</IndexLink>
                </div>

                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul className="nav navbar-nav navbar-right">
                        <NavbarLinkContainer to="/" onlyActiveOnIndex>Home</NavbarLinkContainer>
                        <NavbarLinkContainer to="/about" >About</NavbarLinkContainer>
                        <li className="dropdown">
                            <a href="/" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span className="caret" /></a>
                            <ul className="dropdown-menu">
                                <li><Link to="/about" activeClassName="active">About</Link></li>
                                <li><Link to="/about" activeClassName="active">About</Link></li>
                                <li><Link to="/about" activeClassName="active">About</Link></li>
                                <li role="separator" className="divider" />
                                <li><Link to="/about" activeClassName="active">About</Link></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Header;
