import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import GoalItem from './GoalItem';

describe('<GoalItem />', () => {
    describe('#render', () => {
        it('should return a li with class "list-group-item"', () => {
            const wrapper = shallow(<GoalItem />);
            expect(wrapper.find('li.list-group-item')).to.have.length(1);
        });

        it('should contain a span tag with classes: edit-icon glyphicon glyphicon-pencil pull-right', () => {
            const wrapper = shallow(<GoalItem />);
            expect(wrapper.find('span.edit-icon.glyphicon.glyphicon-pencil.pull-right')).to.have.length(1);
        });
    });
});